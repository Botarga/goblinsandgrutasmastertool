package model;

import java.util.List;

/**
 * Class to manage a G&G pj
 * @author botarga
 */
public class Pj {
    public enum HabilityType{
        STAT, SKILL, NONE
    }
    
    private String                  name;
    private String                  race;
    private String                  profession;
    
    private byte                    strength;
    private byte                    agility;
    private byte                    intelligence;
    private byte                    charisma;
    
    private List<Skill>             skills;
    private List<EquipableObject>   equipment; 
    private List<Item>              items;
    private List<String>            notes;
}
