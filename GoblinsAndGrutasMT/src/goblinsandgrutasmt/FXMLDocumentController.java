/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package goblinsandgrutasmt;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.EquipableObject;
import model.Pj;
import model.Skill;

/**
 *
 * @author botarga
 */
public class FXMLDocumentController implements Initializable {
    @FXML ImageView image;
    
    @FXML private TextField                 nameField;
    @FXML private TextField                 raceField;
    @FXML private TextField                 professionField;
    @FXML private TextField                 pvField;
    @FXML private TextField                 strengthField;
    @FXML private TextField                 agilityField;
    @FXML private TextField                 intelligenceField;
    @FXML private TextField                 charismaField;
    @FXML private TextField                 noteField;
    
    @FXML private RadioButton               checkRadioButton;
    
    @FXML private ListView<EquipableObject> equipmentList;
    @FXML private ListView<String>          notesList;
    
    @FXML private TableView<Skill>          skillTable;
    @FXML private TableColumn               skillColumn;
    @FXML private TableColumn               levelColumn;
    
    
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        image.setImage(new Image("file:img" + File.separator + "banner.jpg"));

    }    
    
    @FXML
    public void deleteSkillAction(ActionEvent e){
        
    }
    
    @FXML
    public void addSkillAction(ActionEvent e){
        
    }
    
    @FXML
    public void createPjAction(ActionEvent e){
        Pj result = null;
        String name, profession, race;
        int pv, strength, agility, intelligence, charisma;
        
        if(validatePj()){
            name = nameField.getText();
            profession = professionField.getText();
            race = raceField.getText();
            
            try{
                pv = Integer.parseInt(pvField.getText());
                
            }catch(Exception ex){
                System.err.println(ex.getMessage());
            }
        }
        
        if(result != null)
            System.out.println("Added PJ");
    }
    
    @FXML
    public void clearAction(ActionEvent e){
        
    }
    
    @FXML
    public void addEquipmentAction(ActionEvent e){
        
    }
    
    @FXML
    public void deleteEquipmentAction(ActionEvent e){
        
    }
    
    @FXML
    public void addNoteAction(ActionEvent e){
        
    }
    
    @FXML
    public void deleteNoteAction(ActionEvent e){
        
    }
    
    
    private void checkCreatePjButton(){
        
    }
    
    
    private boolean validatePj (){
        
        return true;
    }
}
