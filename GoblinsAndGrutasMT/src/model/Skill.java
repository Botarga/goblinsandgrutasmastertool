package model;

/**
 *
 * @author botarga
 */
public class Skill {
    private String  name;
    private byte    value;

    public Skill(String name, byte value) {
        this.name = name;
        this.value = value;
    }
}
